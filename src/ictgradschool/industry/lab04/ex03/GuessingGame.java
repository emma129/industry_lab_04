package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.

        System.out.println("Enter your guess (1 - 100):");
        int goal =(int)((Math.random()*100) + 1) ;
        int guess = Integer.parseInt(Keyboard.readInput());

        while ( guess != goal) {

            if ( guess > goal){

                System.out.println("Too high, try again" + "\n" + "Enter your guess (1 - 100):");
                guess = Integer.parseInt(Keyboard.readInput());

            }if ( guess < goal){

                System.out.println("Too low, try again" + "\n" + "Enter your guess (1 - 100):");
                guess = Integer.parseInt(Keyboard.readInput());
            }



        }if ( guess == goal){

            System.out.println("Perfect!" + "\n" + "Goodbye");


        }





    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
