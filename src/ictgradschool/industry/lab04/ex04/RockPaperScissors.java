package ictgradschool.industry.lab04.ex04;


import ictgradschool.Keyboard;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int Scissors = 2;
    public static final int Paper = 3;
    public static final int Quit = 4;


    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.

        String name = getUserName();
        System.out.println();

        System.out.println("1.Rock" + "\n" + "2.Scissors" + "\n" + "3.Paper" + "\n" + "4.Quit");
        int choice = getUserChoice();
        System.out.println();

        displayPlayerChoice(name, choice);
        int computerChoice = getComputerChoice();
        displayComputerChoice(computerChoice);
        int playerChoice = choice;
        if (playerChoice == computerChoice){

            System.out.println("No one wins");

        }if (computerChoice == 3 && computerChoice - playerChoice == 2){

            System.out.println("The computer wins because " + getResultString(playerChoice,computerChoice) );

        }if (computerChoice == 2 && computerChoice - playerChoice == -1){

            System.out.println("The computer wins because " + getResultString(playerChoice,computerChoice));

        }if (computerChoice == 1 && computerChoice -playerChoice == -1){

            System.out.println("The computer wins because " + getResultString(playerChoice,computerChoice));

        }else if (playerChoice == 3 && userWins(playerChoice,computerChoice)) {

            System.out.println(name + "wins because" + getResultString(playerChoice,computerChoice));

        }if (playerChoice == 2 && userWins(playerChoice,computerChoice)) {

            System.out.println(name + "wins because" + getResultString(playerChoice, computerChoice));
        }if (playerChoice == 1 && userWins(playerChoice,computerChoice)) {

            System.out.println(name + "wins because" + getResultString(playerChoice, computerChoice));
        }




    }


    public String getUserName() {

        System.out.println("Hi! What is your name?");
        String userName = Keyboard.readInput();

        return userName;

    }

    public int getUserChoice() {

        System.out.println("    Enter your choice:");
        int userNumber = Integer.parseInt(Keyboard.readInput());

        return userNumber;


    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)

        System.out.println(name + " chose " + choice);

        if (choice == 4) {

            System.out.println("Goodbye" + name + ". " + "Thanks for playing :) ");

        }


    }

    public int getComputerChoice() {
        int cpChoice = (int) ((Math.random() * 3 + 1));

        return cpChoice;

    }

    public void displayComputerChoice(int computerChoice) {


        System.out.println("Computer chose " + computerChoice);


    }


    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.


        if (playerChoice - computerChoice == -1 || playerChoice - computerChoice == 2 ) {

            return true;

        } else

            return false;

    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.

        if (playerChoice == computerChoice) {

            return TIE;

        }
        if (userWins(playerChoice, computerChoice) && playerChoice == 3
                || !userWins(playerChoice, computerChoice) && computerChoice == 3) {

            return PAPER_WINS;

        }
        if (userWins(playerChoice, computerChoice) && playerChoice == 2 || !userWins(playerChoice, computerChoice) && computerChoice == 2) {

            return SCISSORS_WINS;

        }
        if (userWins(playerChoice, computerChoice) && playerChoice == 1 || !userWins(playerChoice, computerChoice) && computerChoice == 1) {

            return ROCK_WINS;

        } else


            return null;
    }






    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
